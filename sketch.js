var myStore = "alpacadraw";

var dweet_name = "alex";
var dweet_prefix = "alpaca-dance-";

var ps = [];

function preload() {
  // read the initial information
  dweetio.get_latest_dweet_for(
    dweet_prefix + dweet_name,
    function (err, dweet) {
      ps = dweet[0].content; // Dweet is always an array of 1

      console.log(dweet.thing); // The generated name
      console.log(dweet.content); // The content of the dweet
      console.log(dweet.created); // The create date of the dweet
    }
  );
}

var b = 0;

prev = -1;

var sz = 200.0;

function setup() {
  createCanvas(sz, sz + 100);
  b = 240;
  for (i = 0; i < width; ++i) {
    ps.push(0.5);
  }
  input = createInput(dweet_name);
  input.position(20, sz + 40);
  button = createButton("save");
  button.position(input.x, input.y + input.height + 2);
  button.mousePressed(saveline);
  st = createElement("i");
  st.position(button.x, button.y + button.height + 2);
}

function saveline() {
  dweet_name = dweet_prefix + input.value();
  dweetio.dweet_for(dweet_name, ps, function (err, dweet) {
    console.log(dweet.thing); // The generated name
    console.log(dweet.content); // The content of the dweet
    console.log(dweet.created); // The create date of the dweet
  });
  st.html("saved");
}

function draw() {
  background(255);
  fill(b);
  rect(1, 1, 198, 198);

  prevx = -1;
  prevy = -1;

  for (x = 0; x < width; ++x) {
    if (ps[x] == -1) {
      continue;
    }
    if (prevx >= 0) {
      line(prevx, prevy * sz, x, ps[x] * sz);
    }
    prevx = x;
    prevy = ps[x];
  }
}

function mouseDragged(e) {
  if (mouseY >= sz) {
    return;
  }
  const v = mouseY / sz;
  if (prev >= 0) {
    const prevV = ps[prev];
    var diff = prevV - v;

    var start, stop;
    if (prev > mouseX) {
      start = mouseX;
      stop = prev;
    } else {
      stop = mouseX;
      start = prev;
    }
    const dist = stop - start;

    for (var x = start + 1; x < stop; ++x) {
      if (prev > mouseX) {
        ps[x] = v + diff * ((x - start) / dist);
      } else {
        ps[x] = v + diff * (1 - (x - start) / dist);
      }
    }
  }
  ps[mouseX] = mouseY / sz;
  prev = mouseX;
  st.html("");
}

function mouseClicked(e) {
  if (mouseY >= sz) {
    return;
  }
  ps[mouseX] = mouseY / sz;
  prev = mouseX;
  st.html("");
}
